"use strict";

var clipboard = new ClipboardJS('.btn');

var myModal = new bootstrap.Modal(document.getElementById('openInvitationModal'), {})
myModal.toggle() //disable open invitation

particlesJS.load('particles-js', 'upload/config/particles.json', function() {
  console.log('callback - particles.js config loaded');
});


var myAudio = document.getElementById("background_music");
var soundSwitch = document.getElementById("soundSwitch");

var isPlaying = false;

// function togglePlay() {
//   isPlaying ? myAudio.pause() : myAudio.play();
// };

// myAudio.onplaying = function() {
//   isPlaying = true;
//   soundSwitch.checked  = true;
// };
// myAudio.onpause = function() {
//   isPlaying = false;
//   soundSwitch.checked  = false;
// };

// 2. This code loads the IFrame Player API code asynchronously.
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
var player;
function onYouTubeIframeAPIReady() {
  player = new YT.Player('player', {
    height: '390',
    width: '640',
    videoId: 'YHrhoEFXGZM',
    playerVars: {
      'playsinline': 1
    },
    events: {
      'onReady': onPlayerReady,
      'onStateChange': onPlayerStateChange
    }
  });
}

// 4. The API will call this function when the video player is ready.
function onPlayerReady(event) {
  // event.target.playVideo();
}

function onPlayerStateChange(event) {
  if (event.data == YT.PlayerState.PLAYING) {
    myAudio.pause();
  } else{
    myAudio.play();
  }
}
