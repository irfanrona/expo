<!DOCTYPE html>
<?php

// $data = file_get_contents('http://localhost/api/comment');
// $data = file_get_contents('http://api.irona.online/comment');

$data = file_get_contents('https://api.smkbpi.sch.id/comment');

if ($data) {
  $comments = json_decode($data, true);
}


// var_dump($comments);
// foreach ($comments as $c) {
//   echo $c['name'];
// }
// return 0;
?>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="https://www.facebook.com/2008/fbml" lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="robots" content="follow, noindex">
  <meta name="description" content="Minggu, 26 Juni 2022">
  <meta property="og:site_name" content="Undangan BPI EXPO">
  <meta property="og:title" content="Launching BPI EXPO">
  <meta property="og:description" content="Minggu, 26 Juni 2022">
  <meta property="og:url" content="https://bpiexpo.smkbpi.sch.id/">
  <meta property="og:image" content="http://bpiexpo.smkbpi.sch.id/upload/img/landing.jpg">
  <meta property="og:image:secure_url" content="https://bpiexpo.smkbpi.sch.id/upload/img/landing.jpg">
  <meta property="og:image:height" content="250">
  <meta property="og:image:width" content="250">
  <meta property="og:image:type" content="image/jpeg">
  <meta property="og:type" content="article">
  <meta property="og:locale" content="en_US">
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:title" content="LAUNCHING BPI EXPO !!">
  <meta name="twitter:description" content="Minggu, 26 Juni 2022">
  <meta name="twitter:image" content="https://bpiexpo.smkbpi.sch.id/upload/img/landing.jpg">
  <meta name="twitter:label1" content="Ditulis oleh">
  <meta name="twitter:data1" content="SMK BPI Digital Creator">

  <link rel="alternate" type="application/rss+xml" title="Undangan BPI EXPO » Feed" href="https://bpiexpo.smkbpi.sch.id/feed">

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="theme-color" content="#0134d4">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <meta http-equiv='cache-control' content='no-cache'>
  <meta http-equiv='expires' content='0'>
  <meta http-equiv='pragma' content='no-cache'>
  <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <!-- Title -->
  <title>Acara BPI EXPO</title>
  <!-- Fonts -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&amp;display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Parisienne&display=swap" rel="stylesheet">
  <link href="https://fonts.cdnfonts.com/css/brittany-signature" rel="stylesheet">
  
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Kdam+Thmor+Pro&display=swap" rel="stylesheet"> 
  <!-- Favicon -->
  <link rel="icon" href="img/icons/icon-512x512.png">
  <link rel="apple-touch-icon" href="img/icons/icon-96x96.png">
  <link rel="apple-touch-icon" sizes="152x152" href="img/icons/icon-152x152.png">
  <link rel="apple-touch-icon" sizes="167x167" href="img/icons/icon-167x167.png">
  <link rel="apple-touch-icon" sizes="180x180" href="img/icons/icon-180x180.png">
  <!-- CSS Libraries -->
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/bootstrap-icons.css">
  <link rel="stylesheet" href="css/tiny-slider.css">
  <link rel="stylesheet" href="css/baguetteBox.min.css">
  <link rel="stylesheet" href="css/rangeslider.css">
  <link rel="stylesheet" href="css/vanilla-dataTables.min.css">
  <link rel="stylesheet" href="css/apexcharts.css">
  <!-- Core Stylesheet -->
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="wedding.css">
  <!-- Web App Manifest -->
  <link rel="manifest" href="manifest.json">
</head>

<body>
  <!-- Preloader -->
  <div id="preloader">
    <div class="spinner-grow text-primary" role="status"><span class="visually-hidden">Memuat...</span></div>
  </div>
  <!-- Internet Connection Status -->
  <!-- # This code for showing internet connection status -->
  <div class="internet-connection-status" id="internetStatus"></div>
  <!-- Dark mode switching -->
  <div class="dark-mode-switching">
    <div class="d-flex w-100 h-100 align-items-center justify-content-center">
      <div class="dark-mode-text text-center">
        <svg class="bi bi-moon" xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" viewBox="0 0 16 16">
          <path fill-rule="evenodd" d="M14.53 10.53a7 7 0 0 1-9.058-9.058A7.003 7.003 0 0 0 8 15a7.002 7.002 0 0 0 6.53-4.47z"></path>
        </svg>
        <p class="mb-0">Switching to dark mode</p>
      </div>
      <div class="light-mode-text text-center">
        <svg class="bi bi-brightness-high" xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" viewBox="0 0 16 16">
          <path d="M8 11a3 3 0 1 1 0-6 3 3 0 0 1 0 6zm0 1a4 4 0 1 0 0-8 4 4 0 0 0 0 8zM8 0a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-1 0v-2A.5.5 0 0 1 8 0zm0 13a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-1 0v-2A.5.5 0 0 1 8 13zm8-5a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1 0-1h2a.5.5 0 0 1 .5.5zM3 8a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1 0-1h2A.5.5 0 0 1 3 8zm10.657-5.657a.5.5 0 0 1 0 .707l-1.414 1.415a.5.5 0 1 1-.707-.708l1.414-1.414a.5.5 0 0 1 .707 0zm-9.193 9.193a.5.5 0 0 1 0 .707L3.05 13.657a.5.5 0 0 1-.707-.707l1.414-1.414a.5.5 0 0 1 .707 0zm9.193 2.121a.5.5 0 0 1-.707 0l-1.414-1.414a.5.5 0 0 1 .707-.707l1.414 1.414a.5.5 0 0 1 0 .707zM4.464 4.465a.5.5 0 0 1-.707 0L2.343 3.05a.5.5 0 1 1 .707-.707l1.414 1.414a.5.5 0 0 1 0 .708z"></path>
        </svg>
        <p class="mb-0">Switching to light mode</p>
      </div>
    </div>
  </div>
  <!-- Setting Popup Overlay -->
  <div id="setting-popup-overlay"></div>
  <!-- Setting Popup Card -->
  <div class="card setting-popup-card shadow-lg" id="settingCard">
    <div class="card-body">
      <div class="container">
        <div class="setting-heading d-flex align-items-center justify-content-between mb-3">
          <p class="mb-0">Settings</p>
          <div class="btn-close" id="settingCardClose"></div>
        </div>
        <div class="single-setting-panel">
          <div class="form-check form-switch mb-2">
            <input class="form-check-input" type="checkbox" id="darkSwitch">
            <label class="form-check-label" for="darkSwitch">Dark mode</label>
          </div>
          <!-- <div class="form-check form-switch mb-2">
            <input class="form-check-input" type="checkbox" id="soundSwitch" onClick="togglePlay()" checked>
            <label class="form-check-label" for="soundSwitch">Play Song</label>
            <audio id="background_music" loop="loop" src="https://api.our-wedding.link/uploads/4c9081c0-ada5-11eb-a4c4-2be7f1948457.mp3" data-v-2da7ac3a=""></audio>
          </div> -->
        </div>
      </div>
    </div>
  </div>
  <!-- Header Area -->
  <div class="header-area" id="headerArea">
    <div class="container">
      <!-- # Paste your Header Content from here -->
      <!-- # Header Five Layout -->
      <!-- # Copy the code from here ... -->
      <!-- Header Content -->
      <div class="header-content header-style-five position-relative d-flex align-items-center justify-content-between">
        <!-- Logo Wrapper -->
        <div class="logo-wrapper">
        <a href="https://bpiexpo.smkbpi.sch.id"><img src="img/core-img/logo.png" alt=""></a>
        </div>
        <div class="logo-wrapper">BPI EXPO</div>
        <!-- Navbar Toggler -->
        <!-- Settings -->
        <div class="setting-wrapper">
          <div class="setting-trigger-btn" id="settingTriggerBtn">
            <svg class="bi bi-gear" width="18" height="18" viewBox="0 0 16 16" fill="url(#gradientSettings)" xmlns="http://www.w3.org/2000/svg">
              <defs>
                <linearGradient id="gradientSettings" spreadMethod="pad" x1="0%" y1="0%" x2="100%" y2="100%">
                  <stop offset="0" style="stop-color: #c57fe8; stop-opacity:1;"></stop>
                  <stop offset="100%" style="stop-color: #4eace1; stop-opacity:1;"></stop>
                </linearGradient>
              </defs>
              <path fill-rule="evenodd" d="M8.837 1.626c-.246-.835-1.428-.835-1.674 0l-.094.319A1.873 1.873 0 0 1 4.377 3.06l-.292-.16c-.764-.415-1.6.42-1.184 1.185l.159.292a1.873 1.873 0 0 1-1.115 2.692l-.319.094c-.835.246-.835 1.428 0 1.674l.319.094a1.873 1.873 0 0 1 1.115 2.693l-.16.291c-.415.764.42 1.6 1.185 1.184l.292-.159a1.873 1.873 0 0 1 2.692 1.116l.094.318c.246.835 1.428.835 1.674 0l.094-.319a1.873 1.873 0 0 1 2.693-1.115l.291.16c.764.415 1.6-.42 1.184-1.185l-.159-.291a1.873 1.873 0 0 1 1.116-2.693l.318-.094c.835-.246.835-1.428 0-1.674l-.319-.094a1.873 1.873 0 0 1-1.115-2.692l.16-.292c.415-.764-.42-1.6-1.185-1.184l-.291.159A1.873 1.873 0 0 1 8.93 1.945l-.094-.319zm-2.633-.283c.527-1.79 3.065-1.79 3.592 0l.094.319a.873.873 0 0 0 1.255.52l.292-.16c1.64-.892 3.434.901 2.54 2.541l-.159.292a.873.873 0 0 0 .52 1.255l.319.094c1.79.527 1.79 3.065 0 3.592l-.319.094a.873.873 0 0 0-.52 1.255l.16.292c.893 1.64-.902 3.434-2.541 2.54l-.292-.159a.873.873 0 0 0-1.255.52l-.094.319c-.527 1.79-3.065 1.79-3.592 0l-.094-.319a.873.873 0 0 0-1.255-.52l-.292.16c-1.64.893-3.433-.902-2.54-2.541l.159-.292a.873.873 0 0 0-.52-1.255l-.319-.094c-1.79-.527-1.79-3.065 0-3.592l.319-.094a.873.873 0 0 0 .52-1.255l-.16-.292c-.892-1.64.902-3.433 2.541-2.54l.292.159a.873.873 0 0 0 1.255-.52l.094-.319z"></path>
              <path fill-rule="evenodd" d="M8 5.754a2.246 2.246 0 1 0 0 4.492 2.246 2.246 0 0 0 0-4.492zM4.754 8a3.246 3.246 0 1 1 6.492 0 3.246 3.246 0 0 1-6.492 0z"></path>
            </svg><span></span>
          </div>
        </div>
      </div>
      <!-- # Header Five Layout End -->
    </div>
  </div>
  <!-- # Sidenav Left -->
  <div class="page-content-wrapper r-to-top" id="Beranda">
    <!-- CountDown -->
    <div class="coming-soon-wrapper bg-white text-center bg-overlay" style="background-image: url('upload/img/banner_1.JPG')">
      <div class="container">
      <div class="pt-3"></div>
        <p class="text-white lead">Welcoming BPI Digital Transformation</p>
        <div class="text-justify">
        <p class="text-white-50 px-1">Dengan Hormat, <b>Mitra Yayasan BPI</b> </p>
  <p class="text-white-50 px-1">Dengan senang hati kami mengundang Bapa/Ibu/Saudara/i untuk datang dan melihat apa yang dapat ditawarkan oleh sekolah kami: </p>
  </div>
        <h6 class="text-white fw-bold fst-italic display-1 r-font-kdam">BPI</h6>
        <h6 class="text-white fw-bold fst-italic display-2 r-font-kdam">EXPO</h6>

        <p class="text-white-50 fst-italic display-6">Beyond the school</p>
        <div class="text-white-50">26 Juni 2022</div>
        <p class="text-white-50">Jalan Burangrang No. 8, Bandung</p>
        
        
        
        <!-- hapus EXPIRED di id contdown2 -->
        <div class="countdown2 justify-content-center" id="countdown2EXPIRED" data-date="EXPIRED" data-time="09:00">
          <div class="day"><span class="num"></span><span class="word"></span></div>
          <div class="hour"><span class="num"></span><span class="word"></span></div>
          <div class="min"><span class="num"></span><span class="word"></span></div>
          <div class="sec"><span class="num"></span><span class="word"></span></div>
        </div>
        <div class="notify-email mt-1 r-index-3">
          <button class="btn btn-outline-warning r-baca-protokol" type="button" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eye" viewBox="0 0 16 16">
  <path d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.133 13.133 0 0 1 1.66-2.043C4.12 4.668 5.88 3.5 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.133 13.133 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755C11.879 11.332 10.119 12.5 8 12.5c-2.12 0-3.879-1.168-5.168-2.457A13.134 13.134 0 0 1 1.172 8z"/>
  <path d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z"/>
</svg> Baca Protokol</button>
<div class="pt-3"></div>
        </div>
        <!-- Particles -->
        <div class="r-background" id="particles-js"></div>
      </div>
    </div>
    <div class="pt-3"></div>
    <div class="pt-3"></div>
    <!-- Konfirmasi -->
    <div class="container r-to-id" id="Konfirmasi">
      <!-- Contact Form -->
      <div class="card mb-3">
        <div class="card-body">
          <div class="row">
          <?php $total = 0; $hadir = 0; $tidak = 0;
          foreach ($comments as $c) : ?>
            <?php 
            $total++;
            if ($c['attendance'] == 'hadir') { $hadir++;}else{ $tidak++;}

            $persentase = ($hadir / $total) * 100;
          endforeach; ?>
          <span><b><?= $total ?></b> Orang Konfirmasi</span>
                  <div class="progress-bar-wrapper">
                    <div class="progress">
                      <div class="progress-bar bg-warning" style="width: <?= $persentase ?>%;" role="progressbar" aria-valuenow="78" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </div><span><b><?= $hadir ?> Hadir</b>, <?= $tidak ?> Tidak</span>
                
                  
          </div>
        </div>
      </div>
    </div>
    <div class="pt-3"></div>
    <!-- Ucapan dan Doa -->
    <div class="container r-to-id" id="Ucapan">
      <!-- Contact Form -->
      <div class="card mb-3">
        <div class="card-body">
          <h5 class="mb-3">Konfirmasi Kehadiran</h5>
          <div class="contact-form">
            <!-- form action template = http://localhost/api/comment/add -->
            <form action="https://api.smkbpi.sch.id/comment/add" method="POST">
              <div class="form-group mb-3">
                <input class="form-control" type="text" name="name" placeholder="Nama">
              </div>
              <div class="form-group mb-3">
                <input class="form-control" type="hidden" name="relation" placeholder="Asal Sekolah" value="empty">
              </div>
              <div class="form-group mb-3">
                <select class="form-select" name="attendance">
                  <option value="hadir">Hadir</option>
                  <option value="maaf, tidak hadir">Maaf, Tidak Hadir</option>
                </select>
              </div>
              <div class="form-group mb-3">
                <input class="form-control" type="hidden" name="message" placeholder="Nama" value="empty">
              </div>
              <button class="btn btn-primary w-100"><small>Kirim</small></button>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- Chat User List -->
    <div class="container">
      <!-- Element Heading -->
      <div class="element-heading">
        <h6 class="ps-1">Terkonfirmasi</h6>
      </div>

      <div class="data-scrollspy" data-bs-spy="scroll" data-bs-target="#scrollspyWrapper" data-bs-offset="0">
        <ul class="ps-0 chat-user-list">
          <?php foreach ($comments as $c) : ?>
            <!-- TEST Single Chat User -->
            <li class="p-3 <?php if ($c['attendance'] == 'tidak') {
                              echo 'offline';
                            } ?>">
              <a class="d-flex" href="#">
                <!-- Thumbnail -->
                <div class="chat-user-thumbnail me-3 shadow">
                  <span class="img-circle bg-info badge-avater badge-avater-lg"><?= $c['initials'] ?></span>
                  <span class="active-status"></span>
                </div>
                <!-- Info -->
                <div class="chat-user-info px-2">
                  <h6 class=" mt-3"><?= $c['name'] ?> <span class="d-inline-block badge bg-danger mb-1"><?= $c['attendance'] ?></span></h6>
                  <!-- <h6 class=" mb-0"><small>Guru BK</small></h6> -->
                </div>
              </a>
              
            </li>
          <?php endforeach; ?>
        </ul>
      </div>

    </div>
    <div class="pb-3"></div>
  </div>

  <!-- Bootstrap Basic Modal -->
  <div class="modal fade" id="bootstrapBasicModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLabel">Berhasil di salin!</h6>
          <button class="btn btn-close p-1 ms-auto" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
      </div>
    </div>
  </div>
  <!-- Static Backdrop Modal -->
  <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="staticBackdropLabel">Protokol Kesehatan</h6>
          <!-- <button class="btn btn-close p-1 ms-auto" type="button" data-bs-dismiss="modal" aria-label="Close"></button> -->
        </div>
        <div class="modal-body">
          <div class="row">
            <p class="mb-3 text-center"><em>Guna mencegah penyebaran Covid-19, diharapkan bagi tamu undangan untuk mematuhi protokol kesehatan di bawah ini : </em> </p>
          </div>
          <div class="row g-3">
            <div class="col-4">
              <div class="feature-card mx-auto text-center">
                <div class="card mx-auto bg-gray"><img src="upload/icon/cuci-tangan.svg" alt=""></div>
                <p class="mb-0">Cuci Tangan</p>
              </div>
            </div>
            <div class="col-4">
              <div class="feature-card mx-auto text-center">
                <div class="card mx-auto bg-gray"><img src="upload/icon/masker.svg" alt=""></div>
                <p class="mb-0">Gunakan Masker</p>
              </div>
            </div>
            <div class="col-4">
              <div class="feature-card mx-auto text-center">
                <div class="card mx-auto bg-gray"><img src="upload/icon/jarak.svg" alt=""></div>
                <p class="mb-0">Jaga Jarak</p>
              </div>
            </div>
            <div class="col-4">
              <div class="feature-card mx-auto text-center">
                <div class="card mx-auto bg-gray"><img src="upload/icon/kerumunan.svg" alt=""></div>
                <p class="mb-0">Hindari Kerumunan</p>
              </div>
            </div>
            <div class="col-4">
              <div class="feature-card mx-auto text-center">
                <div class="card mx-auto bg-gray"><img src="upload/icon/handsanitizer.svg" alt=""></div>
                <p class="mb-0">Gunakan Handsanitizer</p>
              </div>
            </div>
            <div class="col-4">
              <div class="feature-card mx-auto text-center">
                <div class="card mx-auto bg-gray"><img src="upload/icon/vaksin.svg" alt=""></div>
                <p class="mb-0">Sudah Vaksin</p>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-sm btn-secondary" type="button" data-bs-dismiss="modal">Mengerti</button>
        </div>
      </div>
    </div>
  </div>
  <!-- FULLSCREEN Backdrop Modal FIRST POP UP -->
  <div class="modal fade" id="openInvitationModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="openInvitationModal" aria-hidden="true">
    <div class="modal-dialog modal-fullscreen">
      <div class="modal-content">
        <div class="modal-body text-center bg-img bg-overlay" style="background-image: url('upload/img/landing.JPG');">
          <div class="h-100">
            <div class="row">
              <p class="lead d-block text-opacity-75 pt-5">Undangan Acara</p>
            </div>
            <div class="row pt-5">
              <span class="display-2 text-white fw-lighter fst-italic r-font-kdam">LAUNCHING</span>
              <span class="display-1 text-white fw-lighter fst-italic r-font-kdam">BPI EXPO</span>
              <p class="d-block pt-3"><em>Minggu, 26 Juni 2022</em></p>

            </div>
            <div class="row pt-5">
              <div class="pt-5">
                <p class="d-block text-opacity-75">Kami mengundang <strong>Bapa/Ibu/Saudara/i</strong> untuk hadir di acara kami</p>
                <br>
                <button class="btn btn-outline-warning" type="button" data-bs-dismiss="modal">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-envelope" viewBox="0 0 16 16">
  <path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4Zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2Zm13 2.383-4.708 2.825L15 11.105V5.383Zm-.034 6.876-5.64-3.471L8 9.583l-1.326-.795-5.64 3.47A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.741ZM1 11.105l4.708-2.897L1 5.383v5.722Z"/>
</svg> Buka Undangan
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>

<!-- All JavaScript Files -->
<script src="js/bootstrap.bundle.min.js"></script>
<script src="js/slideToggle.min.js"></script>
<script src="js/internet-status.js"></script>
<script src="js/tiny-slider.js"></script>
<script src="js/baguetteBox.min.js"></script>
<script src="js/countdown.js"></script>
<script src="js/rangeslider.min.js"></script>
<script src="js/vanilla-dataTables.min.js"></script>
<script src="js/index.js"></script>
<script src="js/magic-grid.min.js"></script>
<script src="js/dark-rtl.js"></script>
<script src="js/active.js"></script>
<script src="js/clipboard.min.js"></script>
<script src="js/particles.min.js"></script>
<!-- Function -->
<!-- <script src="js/function.js"></script> -->
<!-- PWA -->
<!-- <script src="js/pwa.js"></script> -->
<script src="js/function.js"></script>
<script>
  console.log(<?= $data ?>);
</script>

</html>
